(function eventos () {
	eventosMenu();
	eventosMenuTrabajador();
	$('#verTrabajadores').on('click', function(){
		console.log("Click en verTrabajadores");
		$("#wait").css("display", "block");
		$("#Region-Editable").load('partials/vertrabajadores.html', function(){
			$("#wait").css("display", "none");
			
		});
		
	});

	$('#cerrarSesion').on('click', function(){
		console.log("Click en cerrarSesion");
		var evento=function(msg){
			if(msg=="true"||msg==true)
				window.open('../login.html' , '_self');
		}
		var data={id:2};
		ajax("../trabajador/controlador/Csesion.php", data, false, true, "cerrar sesion",  evento);
		
		

	});




})();

function eventosMenuTrabajador(){
	console.log("cargando eventos menu trabajador");
	$('#aprobarpostulacion').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/gestionarcursos/aprobarpostulacion.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#modificarfechas').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/gestionarcursos/modificarfechas.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#asignarestatus').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/gestionarcursos/asignarestatus.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#listarcursos').on('click',  function(e){
		console.log("click en aprobarpostulacion");	
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/gestionarcursos/listarcursos.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#Rcursos').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/reportes/cursos.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#RcursosRealizados').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/reportes/cursosrealizados.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#RCursosNoRealizados').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/reportes/cursosnorealizados.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});
	$('#Rrealizadosportrabajadores').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/supervisor/reportes/realizadosportrabajadores.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});





}


function eventosMenu(){
	$('#postularseacurso').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/postularseacurso.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#proponercurso').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/proponercurso.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#Bporfecha').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/BuscarPorFecha.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#Bporcategoria').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/buscarporcategoria.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#Bpornombre').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/buscarpornombre.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});

	$('#vernotificaciones').on('click',  function(e){
			e.preventDefault();
	       	 $("#wait").css("display", "block");
			 $("#Region-Editable").load('../vista/partials/vernotificaciones.html', function(){
			 	$("#wait").css("display", "none");		
			});
	});


}

function ajax( url, data, async, json, nombre, evento){
	//var MSG="";
	if(json){
		$.ajax
			({
			type: "POST",
		   	url: url,
		   	data: data,
			async: async,
			dataType: "json", 				
			success:
		    function (msg) 
			{	
				//MSG=msg;
				evento(msg);
				
		     },
		        error: function(jqXHR, textStatus, errorThrown) {
				  console.log(textStatus, errorThrown + " Error en: "+nombre);
				}
			});	
	}
	else{
		$.ajax
			({
			type: "POST",
		   	url: url,
		   	data: data,
			async: async,
							
			success:
		    function (msg) 
			{	
				//MSG=msg;
				evento(msg);
		     },
		        error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown + " Error en: "+nombre);
				}
			});	
	}
	//return MSG;

}