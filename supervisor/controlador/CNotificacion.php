<?php
	require_once "../modelo/notificaciones.php";
	if(isset($_REQUEST['id'])){
		$i=new CNotificacion();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$i->ObtenerNotificaciones();	
				break;
			case 2:
				$idcurso=$_REQUEST['idcurso'];	
				$fecha=$_REQUEST['fecha'];
				$i->EnviarNotificacion($idcurso, $fecha);	
				break;
			case 3:
				$idcurso=$_REQUEST['idcurso'];	
				$estado=$_REQUEST['estado'];
				$i->EnviarNotificacion_a_Empleado_Status($idcurso, $estado);	
				break;
			case 4:
				$mensaje="Se le Notifica que se ha aprobado la solicitud de postulacion al curso";
				$idinscrito=$_REQUEST['idinscrito'];				
				$i->EnviarNotificacion_a_de_Postulacion($idinscrito, $mensaje);
				break;	
			default;
		}
	}
	class CNotificacion {
		public $n;

		public  function __construct(){
			$this->n=new Notificacion();
		}

		public function EnviarNotificacion_a_de_Postulacion($idinscrito, $mensaje){
			$resul=$this->n->EnviarNotificacion3($idinscrito, $mensaje);
			echo json_encode($resul);
		}
		public function EnviarNotificacion_a_Empleado_Status($idcurso, $estado){
			$resul=$this->n->EnviarNotificacion2($idcurso, $estado);
			echo json_encode($resul);
		}
		public function EnviarNotificacion($idcurso, $fec){
			$resul=$this->n->EnviarNotificacion($idcurso, $fec);
			echo json_encode($resul);
		}

		public function ObtenerNotificaciones(){
			$resul=$this->n->ObtenerNotificaciones();
			echo json_encode($resul);
		}

	}

?>