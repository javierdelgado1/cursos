<?php 
		abstract class BasedeDatos {
			public $host = 'localhost';
			public $user = 'cursos';
			public $pass = '123';
			protected $db = 'postgres';
			protected $query;
			protected $rows = array();
			private $conn;
			# métodos abstractos para ABM de clases que hereden
			/*abstract protected function get();
			abstract protected function set();
			abstract protected function edit();
			abstract protected function delete();*/
			# los siguientes métodos pueden definirse con exactitud
			# y no son abstractos
			# Conectar a la base de datos
			private function conectar() {
				$this->conn =="user='self::$user'  password='self::$pass' port=5432  dbname='$this->db' host='self::$host'";
				$this->conn=pg_connect($this->conn);
			}
			# Desconectar la base de datos
			private function cerrar() {
				pg_close($this->conn);
			}
			# Ejecutar un query simple del tipo INSERT, DELETE, UPDATE
			protected function execute_single_query() {
				$this->conectar();
				pg_query($this->conn, $this->query);
				
				$this->cerrar();
			}
			# Traer resultados de una consulta en un Array
			protected function consultar() {
				$this->conectar();
				$result =pg_query($this->conn, $this->query);
				while ($this->rows[] = pg_fetch_array($result, null, PGSQL_ASSOC));
				
				$this->cerrar();
			}	
		}
?>