<?php
	require_once "../modelo/reportes.php";
	if(isset($_REQUEST['id'])){
		$i=new CReportes();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$i->calcularPaginasdeCursos();				
				break;
			case 2:
				$pagina=$_REQUEST['pagina'];
				$i->obtenerPaginadeCursos($pagina);				
				break;


			case 3:
				$i->calcularPaginasdeCursosRealizados();
				break;
			case 4:
				$pagina=$_REQUEST['pagina'];
				$i->obtenerPaginadeCursosRealizados($pagina);
				break;


			case 5:
				$i->calcularPaginasdeCursosNORealizados();
				break;	
			case 6:
				$pagina=$_REQUEST['pagina'];
				$i->obtenerPaginadeCursosNORealizados($pagina);
				break;

			case 7:	
				$i->calcularPaginasCursosRealizadosPorTrabajadores();
				break;	
			case 8:
				$pagina=$_REQUEST['pagina'];
				$i->obtenerPaginadeCursosRealizadosPorTrabajadores($pagina);
				break;

			case 9:
				$html=$_REQUEST['html'];
				$tipo=$_REQUEST['tipo'];

				$i->declararhtmlcomosession($html, $tipo);
				break;
			default;
		}
	}
	class CReportes {
		public $c;

		public  function __construct(){
			$this->c=new Reportes();
		}
		public function declararhtmlcomosession($html, $tipo){
			$resul=$this->c->declararhtmlcomosession($html, $tipo);
			echo json_encode($resul);
		}
		public function obtenerPaginadeCursosRealizadosPorTrabajadores($pagina){
			$resul=$this->c->obtenerPaginadeCursosRealizadosPorTrabajadores($pagina);
			echo json_encode($resul);
		}
		public function calcularPaginasCursosRealizadosPorTrabajadores(){
			$resul=$this->c->calcularPaginasCursosRealizadosPorTrabajadores();
			echo json_encode($resul);
		}





		public function obtenerPaginadeCursosNORealizados($pagina){
			$resul=$this->c->obtenerPaginadeCursosNORealizados($pagina);
			echo json_encode($resul);
		}
		public function calcularPaginasdeCursosNORealizados(){
			$resul=$this->c->calcularPaginasdeCursosNORealizados();
			echo json_encode($resul);
		}



		public function obtenerPaginadeCursosRealizados($pagina){
			$resul=$this->c->obtenerPaginadeCursosRealizados($pagina);
			echo json_encode($resul);
		}
		public function calcularPaginasdeCursosRealizados(){
			$resul=$this->c->calcularPaginasdeCursosRealizados();
			echo json_encode($resul);
		}



		
		public function calcularPaginasdeCursos(){
			$resul=$this->c->calcularPaginas();
			echo json_encode($resul);
		}
		public function obtenerPaginadeCursos($pagina){
			$resul=$this->c->ObtenerCursossegunPAgina($pagina);
			echo json_encode($resul);

		}

		
	}

/*	$c=new CReportes();
	$c-> obtenerPaginadeCursos(0);*/

	/*$i=new Ccursos();
	ECHO $i->cursos('instrumentacion', '2015-06-24', 'PLC', '40', '123', '1 MES ', '1');	*/
	
?>