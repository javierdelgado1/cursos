<?php
	require_once "conexion.php";

	class Empleado extends BasedeDatos {

		
		public function __construct(){

		}

		public function getEmpleados($idArea){
					
			$query="SELECT c001t_empleados.*, i001t_roles.tx_roles as rol FROM  c001t_empleados 
					INNER JOIN i001t_roles on i001t_roles.co_roles=c001t_empleados.co_roles WHERE co_area='$idArea'";
			$this->resul=$this->consultar($query);
			
			return $this->resul;

		}

		public function getObtenerAreasdeLosEmpleados(){
					
			$query="SELECT * FROM  i002t_area ";
			$this->resul=$this->consultar($query);
			
			return $this->resul;
		}

		public function getEmpleadosqueDictaranCursos(){
					
			$query="SELECT * FROM  c001t_empleados";
			$this->resul=$this->consultar($query);
			
			return $this->resul;
		}



	}
	
 
?>