<?php	

	require_once "conexion.php";
	class Reportes extends BasedeDatos {

		public function declararhtmlcomosession($html, $tipo){
				session_start();
			    $_SESSION['html']=$html;
			    $_SESSION['r']=$tipo;
			    /*echo 	 $_SESSION['html'];*/
				return $html;


		}
		public function obtenerPaginadeCursosRealizadosPorTrabajadores($pagina)
		{
			$pagina=$pagina*10;
			$this->resul2="";
			if($pagina==0){

				$query=" SELECT c001t_empleados.co_empleado as idempleado2, c001t_empleados.tx_nombre_empleado as nombreempleado,
						c001t_empleados.tx_apellido_empleado as apellidoempleado, controlempleado.*  FROM  c001t_empleados 
						LEFT OUTER JOIN  controlempleado ON c001t_empleados.co_empleado=idempleado  LIMIT 10 OFFSET $pagina";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);

			}
			else{
				$desde=$pagina-10;
				$query="SELECT c001t_empleados.co_empleado as idempleado2, c001t_empleados.tx_nombre_empleado as nombreempleado,
						c001t_empleados.tx_apellido_empleado as apellidoempleado, controlempleado.*  FROM  c001t_empleados 
						LEFT OUTER JOIN  controlempleado ON c001t_empleados.co_empleado=idempleado  LIMIT $pagina OFFSET $desde ";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);
			}
			return $this->resul2;
		}


		public function calcularPaginasCursosRealizadosPorTrabajadores(){
			$calcularPaginas="SELECT COUNT(*) as total FROM  c001t_empleados LEFT OUTER JOIN  controlempleado ON c001t_empleados.co_empleado=idempleado";
			$this->resul1=$this->ObtenerColumnas_consulta($calcularPaginas);
			$total=$this->resul1[1]['total'];
			$paginastotales=$total/10;
			$paginastotales=ceil($paginastotales);

			return $paginastotales;
		}













		public function obtenerPaginadeCursosNORealizados($pagina)
		{
			$pagina=$pagina*10;
			$this->resul2="";
			if($pagina==0){

				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado WHERE cursos.idestado='2' ORDER BY idcurso DESC  LIMIT 10 OFFSET $pagina";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);

			}
			else{
				$desde=$pagina-10;
				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado WHERE cursos.idestado='2' ORDER BY idcurso DESC  LIMIT $pagina OFFSET $desde ";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);
			}
			return $this->resul2;
		}


		public function calcularPaginasdeCursosNORealizados(){
			$calcularPaginas="SELECT COUNT(*) as total FROM cursos WHERE idestado='2'";
			$this->resul1=$this->ObtenerColumnas_consulta($calcularPaginas);
			$total=$this->resul1[1]['total'];
			$paginastotales=$total/10;
			$paginastotales=ceil($paginastotales);

			return $paginastotales;
		}
		public function obtenerPaginadeCursosRealizados($pagina)
		{
			$pagina=$pagina*10;
			$this->resul2="";
			if($pagina==0){

				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado WHERE cursos.idestado='1' ORDER BY idcurso DESC  LIMIT 10 OFFSET $pagina";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);

			}
			else{
				$desde=$pagina-10;
				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado WHERE cursos.idestado='1' ORDER BY idcurso DESC  LIMIT $pagina OFFSET $desde ";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);
			}
			return $this->resul2;
		}


		public function calcularPaginasdeCursosRealizados(){
			$calcularPaginas="SELECT COUNT(*) as total FROM cursos WHERE idestado='1'";
			$this->resul1=$this->ObtenerColumnas_consulta($calcularPaginas);
			$total=$this->resul1[1]['total'];
			$paginastotales=$total/10;
			$paginastotales=ceil($paginastotales);

			return $paginastotales;
		}
		public function ObtenerCursossegunPAgina($pagina)
		{
			$pagina=$pagina*10;
			$this->resul2="";
			if($pagina==0){

				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado ORDER BY idcurso DESC  LIMIT 10 OFFSET $pagina";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);

			}
			else{
				$desde=$pagina-10;
				$query="SELECT cursos.*,  estadoscurso.nombre as estado, c001t_empleados.tx_nombre_empleado as nombreempleado, c001t_empleados.tx_apellido_empleado as apellidoempleado FROM cursos 
					    INNER JOIN  c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
					    INNER JOIN  estadoscurso ON estadoscurso.idestado=cursos.idestado ORDER BY idcurso DESC  LIMIT $pagina OFFSET $desde ";			
				$this->resul2=$this->ObtenerColumnas_consulta($query);
			}
			return $this->resul2;
		}

		public function calcularPaginas(){
			$calcularPaginas="SELECT COUNT(*) as total FROM cursos";
			$this->resul1=$this->ObtenerColumnas_consulta($calcularPaginas);
			$total=$this->resul1[1]['total'];
			$paginastotales=$total/10;
			$paginastotales=ceil($paginastotales);

			return $paginastotales;
		}
	}

/*	$r=new Reportes();
	session_start();

     echo 	 $_SESSION['html'];*/

    

?>