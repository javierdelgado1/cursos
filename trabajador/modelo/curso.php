<?php
	
	
	require_once "conexion.php";
	session_start();

	class Curso extends BasedeDatos {

		public function BuscarPorNombre($nombre){
			$query="SELECT cursos.*, c001t_empleados.tx_nombre_empleado as nombredictado, c001t_empleados.tx_apellido_empleado as apellidodictado, estadoscurso.nombre as estado FROM cursos 
					INNER JOIN  estadoscurso ON  estadoscurso.idestado=cursos.idestado 
					INNER JOIN c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
			 		WHERE  cursos.nombre like  '%$nombre%' AND  estadoscurso.nombre='APROBADO'";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;

		}
		public function BuscarPorFecha($fechadesde, $fechahasta){
			$query="SELECT cursos.*, c001t_empleados.tx_nombre_empleado as nombredictado, c001t_empleados.tx_apellido_empleado as apellidodictado, estadoscurso.nombre as estado FROM cursos 
					INNER JOIN  estadoscurso ON  estadoscurso.idestado=cursos.idestado 
					INNER JOIN c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
			 		WHERE  fecha BETWEEN '$fechadesde' AND '$fechahasta' AND estadoscurso.nombre='APROBADO' ";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;

		}
		public function verificar_si_esta_inscrito($idcurso){
			$empleado=$_SESSION['co_empleado'];			
			$query="SELECT COUNT(*) as inscrito FROM  inscritos WHERE  idcurso='$idcurso' AND idempleado='$empleado'";
			/*$this->resul=$this->ObtenerColumnas_consulta($query);*/
			$this->resul=$this->consultar($query);	
			$o[0]['m']=-1;
			if($line = pg_fetch_assoc($this->resul)){
				$o[0]['m']=$line['inscrito'];
			    
			}		
			return $o;	

		}
		public function inscribirse_a_curso($idcurso){
			$empleado=$_SESSION['co_empleado'];
			$fecha=date("Y-m-d");
			$query="INSERT INTO inscritos (idempleado, idcurso, fecha, aprobado)  VALUES ('$empleado', '$idcurso', '$fecha', '0')";
			$this->resul=$this->consultar($query);	
			return "true";	

		}
		public function cuposDisponibles($idcurso){
			$query="SELECT COUNT(*) as inscritos FROM  inscritos WHERE  idcurso='$idcurso'";
			$this->resul=$this->consultar($query);	
			$o[0]['m']=-1;
			if($line = pg_fetch_assoc($this->resul)){
				$o[0]['m']=$line['inscritos'];
			    
			}		
			return $o;	

		}
		public function BuscarCursosPorCategoria($idcategoria){
			$query="SELECT cursos.*, c001t_empleados.tx_nombre_empleado as nombredictado, c001t_empleados.tx_apellido_empleado as apellidodictado, estadoscurso.nombre as estado FROM cursos 
					INNER JOIN  estadoscurso ON  estadoscurso.idestado=cursos.idestado 
					INNER JOIN c001t_empleados ON c001t_empleados.co_empleado=cursos.iddictadopor
			 WHERE  idcategoria='$idcategoria' AND  estadoscurso.nombre='APROBADO'";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;	
		}


		public function cambiarFecha($fecha, $idcurso){			
			$this->salida="true";
			$query="UPDATE cursos SET  fecha='$fecha' WHERE idcurso='$idcurso'";
			$this->resul=$this->consultar($query);			
			return $this->salida;
		}
		public function obtenerEstados_cursos(){	
			$query="SELECT * FROM  estadoscurso ORDER BY idestado DESC ";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;
		}
		public function obtenerProponerCurso($idcurso){			
			$query="SELECT * FROM  cursos WHERE  idcurso='$idcurso'";
			$this->resul=$this->consultar($query);			
			return $this->resul;			
		}
		public function cambiarEstado($curso, $estado){				
			$this->salida="true";	
			$query="UPDATE cursos SET idestado='$estado' WHERE  idcurso='$curso'";
			$this->resul=$this->consultar($query);	

			if($estado==1){
				$BuscarEmpleado="SELECT dictadopor FROM  cursos  WHERE  idcurso='$curso'";
				$this->resul2=$this->consultar($BuscarEmpleado);
				$rows[0]['m']=pg_num_rows($this->resul2);
				$idempleado=$rows[1]['dictadopor'];
				$ActualizarControl=" UPDATE controlempleado  SET cursospropuestos=cursospropuestos+1  WHERE idempleado='$idempleado'";
				$this->resul3=$this->consultar($ActualizarControl);
			}	
			if($estado==2){
				$BuscarEmpleado="SELECT dictadopor FROM  cursos  WHERE  idcurso='$curso'";
				$this->resul2=$this->consultar($BuscarEmpleado);
				$rows[0]['m']=pg_num_rows($this->resul2);
				$idempleado=$rows[1]['dictadopor'];
				$ActualizarControl=" UPDATE controlempleado  SET cursospropuestos=cursospropuestos-1  WHERE idempleado='$idempleado'";
				$this->resul3=$this->consultar($ActualizarControl);
			}	

			return $this->salida;
		}
		public function ProponerCurso($nombre, $fecha, $sitio, $cupos,  $duracion, $categoria){			
			$this->salida="true";	
			$dictadopor=$_SESSION['co_empleado'];
			$query1="SELECT * FROM cursos WHERE  fecha='$fecha'";
			$this->resul1=$this->consultar($query1);
			$rows1[0]['m']=pg_num_rows($this->resul1);
			//$this->salida=$rows1[0]['m'];
			if($rows1[0]['m']==0){				
				$query="INSERT INTO cursos (nombre, fecha, sitio, cupos, iddictadopor, duracion, idestado, idcategoria)
						VALUES('$nombre', '$fecha', '$sitio', '$cupos', '$dictadopor', '$duracion', '3', '$categoria')";
				$this->resul=$this->consultar($query);	

				$query2="SELECT * FROM  controlempleado WHERE  idempleado='$dictadopor'";	
				$this->resul2=$this->consultar($query2);
				$rows[0]['m']=pg_num_rows($this->resul2);
				if($rows[0]['m']==0){
					$query3="INSERT INTO controlempleado (idempleado, cursosrealizados, cursospropuestos, cursosdictados) VALUES ('$dictadopor', '0', '0', '0')";
					$this->resul3=$this->consultar($query3);

				}
			}	
			else{
				$this->salida="repetido";

			}

			return $this->salida;
		}


		public function ObtenerPropuestas($estado){			
			$query="SELECT cursos.*, estadoscurso.nombre as nombreestado FROM  cursos  INNER JOIN estadoscurso on estadoscurso.idestado=cursos.idestado  ORDER BY idcurso DESC";
			$this->resul=$this->consultar($query);			
			return $this->resul;
		}


	}
	/*$c=new Curso();
	print_r($c->inscribirse_a_curso(9));*/
	
?>