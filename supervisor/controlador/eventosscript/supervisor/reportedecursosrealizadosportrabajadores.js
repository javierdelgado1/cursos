(function main(){
	console.log("hola mundo: reportes cursos realizados por trabajadores");
	obtenerPaginacion();
	abrirPagina(0);
	exportar();
	
})();

function verEmpleado(){
	$('.empleado').on('click', function(){
		var idempleado=$(this).attr('name');
		window.open("controlempleado.php?id="+idempleado, "", "left=100, top=100  width=850, height=400");
	});

}	

function exportar(){
	$('#exportar').on('click', function(){

		var html = $('#Region-Editable').html();

		var evento=function(msg){
			/*console.log(msg);*/
			window.open("pdfreporte.php");	
		}
		var data={id:9, html:html, tipo:4};
		ajax("../supervisor/controlador/Creportes.php", data, false,false, "enviar html como variable de session",  evento);
	});	

	
}

function obtenerPaginacion(){
	var evento=function(msg){
		var salida="";
		if(msg==0){
			$('#Contenedor-Elemento-Formulario-aux').hide();
		}
		else{
			$('#Contenedor-Elemento-Formulario-aux').show();
		}
		for (var i =1; i<=msg; i++) {
			salida+='<a href="#" class="pagina" name="'+i+'" > '+i+' </a>';			
		}	
		paginacion.innerHTML=salida;
		eventoclickPagina();
	}
	var data={id:7};
	ajax("../supervisor/controlador/Creportes.php", data, false,true, "Obtener Paginacion",  evento);
}

function eventoclickPagina(){
	$('.pagina').on('click', function(){
		var pagina=$(this).attr('name');
		abrirPagina(pagina);
	});	
}

function abrirPagina(pagina){
		var evento=function(msg){
		  $('#tabla').html(""); 
			ListarTabla(msg);
			verEmpleado();
		}
		var data={id:8, pagina:pagina};
		ajax("../supervisor/controlador/Creportes.php", data, false,true, "abrirpagina",  evento);
}
function ListarTabla(msg){         
		var row1=$('<tr></tr>');
		var fila0=$('<th></th>').text("Codigo Empleado");
		var fila1=$('<th></th>').text("Nombre");
		var fila2=$('<th></th>').text("Apellido");		
		var fila3=$('<th></th>').text("Cursos realizados");
		/*var fila4=$('<th></th>').text("Cupos");*/
		var fila5=$('<th></th>').text("Cursos Dictados");
		/*var fila6=$('<th></th>').text("Duracion");*/
		/*var fila7=$('<th></th>').text("Categoria");*/
		var fila8=$('<th></th>').text("Estado");
		row1.append(fila0);
		row1.append(fila1);
		row1.append(fila2);
		row1.append(fila3);		
		/*row1.append(fila4);*/
		row1.append(fila5);
		/*row1.append(fila6);*/
		/*row1.append(fila7); */
		/*row1.append(fila8); */


		$('#tabla').append(row1);
        for(i=1; i<=msg[0].m; i++){                   
                var row3=$('<tr></tr>');
                var fila0=$('<td></td>').html('<a href="#" class="empleado" name="'+msg[i].idempleado2+'">'+msg[i].idempleado2+'</a>');
                var fila1=$('<td></td>').text(msg[i].nombreempleado);
                var fila2 = $('<td></td>').text(msg[i].apellidoempleado);  
                var cursosrealizados=msg[i].cursosrealizados;
            //    console.log(cursosrealizados);
                if(cursosrealizados==null)
                	cursosrealizados=0;           
                var fila3 = $('<td></td>').text(cursosrealizados);
               /* var fila4 = $('<td></td>').text(msg[i].cupos);   */  
                var cursospropuestos=msg[i].cursospropuestos;
                if(cursospropuestos==null)
                	cursospropuestos=0;          
                var fila5 = $('<td></td>').text(cursospropuestos);                
                /*var fila6 = $('<td></td>').text(msg[i].duracion); */ 
                /*var fila7 = $('<td></td>').text(msg[i].categoria); */               
                var fila8 = $('<td></td>').text(msg[i].estado);                


               	row3.append(fila0);
                row3.append(fila1);
                row3.append(fila2);               
                row3.append(fila3);  
               /* row3.append(fila4);*/
                row3.append(fila5);
               /* row3.append(fila6);*/
                /*row3.append(fila7);*/
               /* row3.append(fila8);*/


				$('#tabla').append(row3);           
        }		
}