<?php
	require_once "../modelo/empleado.php";
	$id=$_REQUEST['id'];
	$e=new Cempleados();
	switch($id)
	{
		case 1:			
			$e->getAreas();
			break;
		case 2:
			$idarea=$_REQUEST['idarea'];
			$e->getEmpleadosdeunArea($idarea);
			break;
		case 3:
			$e->getEmpleadosquepuedenDictarCursos();
			break;
		default;
	}


	class Cempleados {
		public $e;  
		public function __construct(){
			$this->e=new Empleado();
		}

		public function getEmpleadosquepuedenDictarCursos(){
			
			$resul=$this->e->getEmpleadosqueDictaranCursos();
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			while ($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idempleado']=$reg['co_empleado'];
			     $o[$i]['nombreempleado']=$reg['tx_nombre_empleado']." ".$reg['tx_apellido_empleado'];

			    $i++;
			}

			echo json_encode($o);
		}
		public function getAreas(){
			$e=new Empleado();
			$resul=$this->e->getObtenerAreasdeLosEmpleados();
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			while ($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idarea']=$reg['co_area'];
			     $o[$i]['nombrearea']=$reg['tx_area'];

			    $i++;
			}

			echo json_encode($o);
		}

		public function getEmpleadosdeunArea($idarea){
			$e=new Empleado();
			$result=$this->e->getEmpleados($idarea);
			$o[0]['m']=pg_numrows($result);
			$i=0;
			while ($reg = pg_fetch_array($result, null, PGSQL_ASSOC)) {
			     $o[$i]['idarea']=$reg['co_area'];
			     $o[$i]['codigoempleado']=$reg['co_empleado'];
			     $o[$i]['nombre']=$reg['tx_nombre_empleado'];
			     $o[$i]['apellido']=$reg['tx_apellido_empleado'];
			     $o[$i]['indicador']=$reg['tx_indicador'];
			     $o[$i]['rol']=$reg['rol'];


			    $i++;
			}

			echo json_encode($o);
		}

	}

/*	$e=new  Cempleados();
	$e->getEmpleadosdeunArea(1);*/
?>