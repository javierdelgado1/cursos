<?php
	require_once "../modelo/curso.php";
	if(isset($_REQUEST['id'])){
		$i=new Ccursos();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$nombre=$_REQUEST['nombre'];
				$fecha=$_REQUEST['fecha'];
				$sitio=$_REQUEST['sitio'];
				$cupos=$_REQUEST['cupos'];
				//$dictadopor=$_REQUEST['dictadopor'];
				$duracion=$_REQUEST['duracion'];
				$categoria=$_REQUEST['categoria'];
				$i->cursos($nombre, $fecha, $sitio, $cupos, $duracion, $categoria);	
				break;
			case 2:
				$estado=$_REQUEST['estado'];				
				$i->obtenerPropuestas($estado);	
				break;
			case 3:
				$idcurso=$_REQUEST['idcurso'];
				$estado=$_REQUEST['estado'];
				$i->cambiarestado($idcurso,$estado);
				break;
			case 4:
				$idcurso=$_REQUEST['idcurso'];
				$i->ObtenerCurso($idcurso);
				break;
			case 5:
				$i->obtenerEstados();
				break;
			case 6:
				$idcurso=$_REQUEST['idcurso'];
				$fecha=$_REQUEST['fecha'];
				$i->cambiarFecha($fecha, $idcurso);
				break;
			case 7:
				$idcategoria=$_REQUEST['categoria'];
				$i->buscarCursosPorCategoria($idcategoria);
				break;
			case 8:
				$idcurso=$_REQUEST['idcurso'];
				$i->buscarinscritos($idcurso);
				break;
			case 9:
				$idcurso=$_REQUEST['idcurso'];
				$i->postularse($idcurso);
				break;
			case 10:
				$idcurso=$_REQUEST['idcurso'];
				$i->verificarsiestainscrito($idcurso);
				break;
			case 11:
				$fechadesde=$_REQUEST['fechadesde'];
				$fechahasta=$_REQUEST['fechahasta'];
				$i->BuscarPorFecha($fechadesde, $fechahasta);
				break;
			case 12:
				$nombre=$_REQUEST['nombre'];
				$i->BuscarPorNombre($nombre);
				break;

			default;
		}
	}
	class Ccursos {
		public $c;

		public  function __construct(){
			$this->c=new Curso();
		}

		public function BuscarPorNombre($nombre){
			$resul=$this->c->BuscarPorNombre($nombre);
			echo json_encode($resul);
		}
		public function BuscarPorFecha($fechadesde, $fechahasta){
			$resul=$this->c->BuscarPorFecha($fechadesde, $fechahasta);
			echo json_encode($resul);
		}


		public function verificarsiestainscrito($idcurso){
			$resul=$this->c->verificar_si_esta_inscrito($idcurso);
			echo json_encode($resul);
		}
		public function postularse($idcurso){
			$resul=$this->c->inscribirse_a_curso($idcurso);
			echo json_encode($resul);

		}
		public function buscarinscritos($idcurso){
			$resul=$this->c->cuposDisponibles($idcurso);
			echo json_encode($resul);

		}
		public function buscarCursosPorCategoria($categoria){
			$resul=$this->c->BuscarCursosPorCategoria($categoria);
			echo json_encode($resul);
			
		}

		public function cambiarFecha($fecha, $idcurso){
			$resul=$this->c->cambiarFecha($fecha, $idcurso);
			echo json_encode($resul);
		}
		public function obtenerEstados(){
			$resul=$this->c->obtenerEstados_cursos();
			/*$o[0]['m']=pg_numrows($resul);
			$i=0;
			while($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idestado']=$reg['idestado'];
			     $o[$i]['nombre']=$reg['nombre'];
			     $i++;	   
			}*/

			echo json_encode($resul);
		}
		public function ObtenerCurso($idcurso){
			$resul=$this->c->obtenerProponerCurso($idcurso);
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			if($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idcurso']=$reg['idcurso'];

			     $o[$i]['nombre']=$reg['nombre'];
			      $o[$i]['fecha2']=$reg['fecha'];
			     $o[$i]['fecha']=$reg['fecha'];
			     $date = new DateTime( $o[$i]['fecha']);
				 $o[$i]['fecha']=$date->format('d-m-Y');
			     $o[$i]['sitio']=$reg['sitio'];
			     $o[$i]['cupos']=$reg['cupos'];
			     $o[$i]['iddictadopor']=$reg['iddictadopor'];
			     $o[$i]['duracion']=$reg['duracion'];			     
			     $o[$i]['idcategoria']=$reg['idcategoria'];
			     $o[$i]['idestado']=$reg['idestado'];

			}


			echo json_encode($o);
		}
		public function cambiarestado($curso, $estado){
			$resul=$this->c->cambiarEstado($curso, $estado);
			echo json_encode($resul);

		}
		public function obtenerPropuestas($estado){
			$resul=$this->c->ObtenerPropuestas($estado);
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			while ($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idcurso']=$reg['idcurso'];

			     $o[$i]['nombre']=$reg['nombre'];
			     $o[$i]['fecha']=$reg['fecha'];
			     $date = new DateTime( $o[$i]['fecha']);
				 $o[$i]['fecha']=$date->format('d-m-Y');

			     $o[$i]['sitio']=$reg['sitio'];
			     $o[$i]['cupos']=$reg['cupos'];
			     $o[$i]['iddictadopor']=$reg['iddictadopor'];
			     $o[$i]['duracion']=$reg['duracion'];
			     $o[$i]['nombreestado']=$reg['nombreestado'];
			     $o[$i]['idcategoria']=$reg['idcategoria'];

			    $i++;
			}


			echo json_encode($o);
		}	
		public function cursos($nombre, $fecha, $sitio, $cupos, $duracion, $categoria){

			$resul=$this->c->proponerCurso($nombre, $fecha, $sitio, $cupos, $duracion, $categoria);

			echo json_encode($resul);
		}
	}

/*	$c=new Ccursos();
	
	echo $c->buscarCursosPorCategoria(10);*/


	/*$i=new Ccursos();
	ECHO $i->cursos('instrumentacion', '2015-06-24', 'PLC', '40', '123', '1 MES ', '1');	*/
	
?>