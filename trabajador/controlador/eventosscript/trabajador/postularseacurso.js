(function main(){
		console.log("Hola mundo : Postularse a curso");
		$("#wait").css("display", "none");
		cargarCategorias();
})();

function cargarCategorias(){
		var evento=function(msg){
				//console.log(msg);
				    $('#Scategoria').empty();           
                     $('#Scategoria').append(' <option value="-1" disabled selected> --Categorias -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                          $('#Scategoria').append('<option value="'+msg[i].idcategoria+'" >'+msg[i].nombrecategoria+'</option>');
            		}
            		setTimeout(function(){
                		$("#wait").css("display", "none");
                	}, 1000);
            		//mostrardictadores();
                	mostrar();
		}
		var data={id:1};
		ajax("../trabajador/controlador/Ccategorias.php", data, false, true, "Obtener Categorias",  evento);
}

function mostrar(){	
   	$('#Scategoria').change(function () {
   		 buscarcursosporCategoria($('#Scategoria').val());
   	});
}
function buscarcursosporCategoria(idcategoria){
	var evento=function(msg){
		//console.log(msg);
		 $('#tabla').html("");   	 	                  
         ListarTabla(msg);  
         Postularse();        
          /*VermasDetalles();         
         cargarCategorias();*/
	}
	var data={id:7, categoria:idcategoria};
	ajax("../trabajador/controlador/Ccurso.php", data, true,true, "Obtener cursos por categoria",  evento);
}

function Postularse(){
	$('.postularse').on('click', function(){
		$("#wait").css("display", "block");
		var id=$(this).attr('name');	
	
		var evento=function(msg){
			console.log(msg);
			$("#wait").css("display", "none");
			buscarcursosporCategoria($('#Scategoria').val());
		}
		var data={id:9, idcurso:id};
		ajax("../trabajador/controlador/Ccurso.php", data, true,false, "Postularse a curso",  evento);
	});
}
function ListarTabla(msg){         
		var row1=$('<tr></tr>');
		var fila00=$('<th></th>').text("Nº");
		var fila0=$('<th></th>').text("Nombre");
		var fila1=$('<th></th>').text("Fecha");		
		var fila3=$('<th></th>').text("Cupos Disponibles");
		var fila4=$('<th></th>').text("Dictado por:");
		var fila5=$('<th></th>').text("Postularse");
		row1.append(fila00);
		row1.append(fila0);
		row1.append(fila1);		
		row1.append(fila3);
		row1.append(fila4); 
		row1.append(fila5); 
		$('#tabla').append(row1);
		resultado.innerHTML='Mostrando '+msg[0].m+' resultados encontrados';
        for(i=1; i<=msg[0].m; i++){                   
                var row3=$('<tr></tr>');
                var fila00=$('<td></td>').text(msg[i].idcurso);
                var fila0=$('<td></td>').text(msg[i].nombre);
                var fila1 = $('<td></td>').text(msg[i].fecha); 
                var cupos=msg[i].cupos;
                var evento=function(msg2){
					//console.log("Curso: "+msg[i].idcurso+ " Inscritos: "+msg2[0].m);
					cupos=cupos-msg2[0].m;
				}
				var data={id:8, idcurso:msg[i].idcurso};
				ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Obtener inscritos dado un curso",  evento);           
                var fila3 = $('<td></td>').text(cupos);                
                var fila4 = $('<td></td>').text(msg[i].nombredictado + " "+ msg[i].apellidodictado);                
                
                var inscrito=false;
				 var fila5 = "";
              	var evento=function(msg3){
					console.log(msg3);
					if(msg3[0].m>0){
                		inscrito=true;

					}
					if(inscrito){
	                	fila5 = $('<td></td>').html('<button   title="Empleado Inscrito en este curso" name="'+msg[i].idcurso+'" disabled="true">Inscrito</button>');

					}	
					else if(cupos>0){
	                	fila5 = $('<td></td>').html('<button class="postularse"  title="Postularse a Curso" name="'+msg[i].idcurso+'" >Postularse</button>');

					}
					else 
	                	fila5 = $('<td></td>').html('<button   title="Sin cupo" name="'+msg[i].idcurso+'" disabled="true">Sin cupo</button>');
						
				}
				var data={id:10, idcurso:msg[i].idcurso};
				ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Verificar si esta inscrito",  evento); 

                row3.append(fila00);
                row3.append(fila0);
                row3.append(fila1);               
                row3.append(fila3);  
                row3.append(fila4);
                row3.append(fila5);
				$('#tabla').append(row3);    
				//break;       
        }		
}