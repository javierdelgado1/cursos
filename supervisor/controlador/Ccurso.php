<?php
	require_once "../modelo/curso.php";
	if(isset($_REQUEST['id'])){
		$i=new Ccursos();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$nombre=$_REQUEST['nombre'];
				$fecha=$_REQUEST['fecha'];
				$sitio=$_REQUEST['sitio'];
				$cupos=$_REQUEST['cupos'];
				$dictadopor=$_REQUEST['dictadopor'];
				$duracion=$_REQUEST['duracion'];
				$categoria=$_REQUEST['categoria'];
				$i->cursos($nombre, $fecha, $sitio, $cupos, $dictadopor, $duracion, $categoria);	
				break;
			case 2:
				$estado=$_REQUEST['estado'];				
				$i->obtenerPropuestas($estado);	
				break;
			case 3:
				$idcurso=$_REQUEST['idcurso'];
				$estado=$_REQUEST['estado'];
				$i->cambiarestado($idcurso,$estado);
				break;
			case 4:
				$idcurso=$_REQUEST['idcurso'];
				$i->ObtenerCurso($idcurso);
				break;
			case 5:
				$i->obtenerEstados();
				break;
			case 6:
				$idcurso=$_REQUEST['idcurso'];
				$fecha=$_REQUEST['fecha'];
				$i->cambiarFecha($fecha, $idcurso);
				break;
			case 7:
				$i->ObtenerPostulaciones();
				break;
			case 8:
				$idinscrito=$_REQUEST['idinscrito'];
				$aprobado=$_REQUEST['aprobado'];
				$i->AprobarSolicitud_De_Postulacion($idinscrito, $aprobado);
				break;
			case 9:
				$estado=$_REQUEST['estado'];				
				$i->obtenerPropuestas2($estado);	
				break;
			case 10:
				$idinscrito=$_REQUEST['idinscrito'];
				$i->Liberar($idinscrito);
				break;
			default;
		}
	}
	class Ccursos {
		public $c;

		public  function __construct(){
			$this->c=new Curso();
		}
		public function Liberar($idinscrito){
			$resul=$this->c->Liberar($idinscrito);
			echo json_encode($resul);
		}
		public function obtenerPropuestas2($estado){
			$resul=$this->c->ObtenerPropuestasAprobados($estado);
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			while ($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idcurso']=$reg['idcurso'];
			     $o[$i]['nombre']=$reg['nombre'];
			     $o[$i]['fecha']=$reg['fecha'];
			     $date = new DateTime( $o[$i]['fecha']);
				 $o[$i]['fecha']=$date->format('d-m-Y');
			     $o[$i]['sitio']=$reg['sitio'];
			     $o[$i]['cupos']=$reg['cupos'];
			     $o[$i]['iddictadopor']=$reg['iddictadopor'];
			     $o[$i]['duracion']=$reg['duracion'];
			     $o[$i]['nombreestado']=$reg['nombreestado'];
			     $o[$i]['idcategoria']=$reg['idcategoria'];
			    $i++;
			}
			echo json_encode($o);
		}	
		public function AprobarSolicitud_De_Postulacion($idinscrito, $aprobado){
			$resul=$this->c->aprobar($idinscrito, $aprobado);
			echo json_encode($resul);
		}
		public function ObtenerPostulaciones(){
			$resul=$this->c->ObtenerPostulaciones();
			echo json_encode($resul);
		}
		public function cambiarFecha($fecha, $idcurso){
			$resul=$this->c->cambiarFecha($fecha, $idcurso);
			echo json_encode($resul);
		}
		public function obtenerEstados(){
			$resul=$this->c->obtenerEstados_cursos();
			/*$o[0]['m']=pg_numrows($resul);
			$i=0;
			while($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idestado']=$reg['idestado'];
			     $o[$i]['nombre']=$reg['nombre'];
			     $i++;	   
			}*/

			echo json_encode($resul);
		}
		public function ObtenerCurso($idcurso){
			$resul=$this->c->obtenerProponerCurso($idcurso);
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			if($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idcurso']=$reg['idcurso'];

			     $o[$i]['nombre']=$reg['nombre'];
			      $o[$i]['fecha2']=$reg['fecha'];
			     $o[$i]['fecha']=$reg['fecha'];
			     $date = new DateTime( $o[$i]['fecha']);
				 $o[$i]['fecha']=$date->format('d-m-Y');
			     $o[$i]['sitio']=$reg['sitio'];
			     $o[$i]['cupos']=$reg['cupos'];
			     $o[$i]['iddictadopor']=$reg['iddictadopor'];
			     $o[$i]['duracion']=$reg['duracion'];			     
			     $o[$i]['idcategoria']=$reg['idcategoria'];
			     $o[$i]['idestado']=$reg['idestado'];

			}


			echo json_encode($o);
		}
		public function cambiarestado($curso, $estado){
			$resul=$this->c->cambiarEstado($curso, $estado);
			echo json_encode($resul);

		}
		public function obtenerPropuestas($estado){
			$resul=$this->c->ObtenerPropuestas($estado);
			$o[0]['m']=pg_numrows($resul);
			$i=0;
			while ($reg = pg_fetch_array($resul, null, PGSQL_ASSOC)) {
			     $o[$i]['idcurso']=$reg['idcurso'];

			     $o[$i]['nombre']=$reg['nombre'];
			     $o[$i]['fecha']=$reg['fecha'];
			     $date = new DateTime( $o[$i]['fecha']);
				 $o[$i]['fecha']=$date->format('d-m-Y');

			     $o[$i]['sitio']=$reg['sitio'];
			     $o[$i]['cupos']=$reg['cupos'];
			     $o[$i]['iddictadopor']=$reg['iddictadopor'];
			     $o[$i]['duracion']=$reg['duracion'];
			     $o[$i]['nombreestado']=$reg['nombreestado'];
			     $o[$i]['idcategoria']=$reg['idcategoria'];

			    $i++;
			}


			echo json_encode($o);
		}	
		public function cursos($nombre, $fecha, $sitio, $cupos, $dictadopor, $duracion, $categoria){

			$resul=$this->c->proponerCurso($nombre, $fecha, $sitio, $cupos, $dictadopor, $duracion, $categoria);

			echo json_encode($resul);
		}
	}

/*	$c=new Ccursos();
	$c->obtenerPropuestas(1);*/

	/*$i=new Ccursos();
	ECHO $i->cursos('instrumentacion', '2015-06-24', 'PLC', '40', '123', '1 MES ', '1');	*/
	
?>