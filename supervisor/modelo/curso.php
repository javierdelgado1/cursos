<?php


	require_once "conexion.php";
	class Curso extends BasedeDatos {
		public function Liberar($idinscrito){
			$query="DELETE FROM  inscritos WHERE  idinscrito='$idinscrito'";
			$this->resul=$this->ObtenerColumnas_consulta($query);
			return true;

		}
		public function aprobar($idinscrito, $aprobado){
			$this->salida="true";		

			$query2="SELECT idempleado FROM inscritos WHERE  idinscrito='$idinscrito'";
			$this->resul2=$this->ObtenerColumnas_consulta($query2);		
			$idempleado=$this->resul2[1]['idempleado'];

			if($aprobado==1){				
				$query3="SELECT * FROM controlempleado WHERE  idempleado='$idempleado'";
				$this->resul3=$this->ObtenerColumnas_consulta($query3);

				if($this->resul3[0]['m']==0){
					$registrarControl="INSERT INTO  controlempleado (idempleado, cursosrealizados, cursospropuestos, cursosdictados)
							 VALUES ('$idempleado', '1', '0', '0')";
					$this->resul=$this->consultar($registrarControl);			
				}
				else if($this->resul3[1]['cursosrealizados']<3){
					$Actualizarcursosrealizados="UPDATE controlempleado  SET cursosrealizados=cursosrealizados+1 WHERE idempleado='$idempleado'";
					$this->resul=$this->consultar($Actualizarcursosrealizados);		

					$query="UPDATE inscritos SET  aprobado='1' WHERE idinscrito='$idinscrito'";
					$this->resul=$this->consultar($query);				
				}			
				else{
					$this->salida="El Empleado ya ha realizado los 3 cursos correspondientes ha este año";
				}
			}
			else{

				$query3="SELECT * FROM controlempleado WHERE  idempleado='$idempleado'";
				$this->resul3=$this->ObtenerColumnas_consulta($query3);

				if($this->resul3[0]['m']==0){
					$registrarControl="INSERT INTO  controlempleado (idempleado, cursosrealizados, cursospropuestos, cursosdictados)
							 VALUES ('$idempleado', '0', '0', '0')";
					$this->resul=$this->consultar($registrarControl);			
				}
				else if($this->resul3[1]['cursosrealizados']>0){
					$Actualizarcursosrealizados="UPDATE controlempleado  SET cursosrealizados=cursosrealizados-1 WHERE idempleado='$idempleado'";
					$this->resul=$this->consultar($Actualizarcursosrealizados);		

					$query="UPDATE inscritos SET  aprobado='0' WHERE idinscrito='$idinscrito'";
					$this->resul=$this->consultar($query);				
				}			
				else{
					$this->salida="El Empleado ya ha realizado los 3 cursos correspondientes ha este año";
				}


			}


			return $this->salida;
		}
		public function ObtenerPostulaciones(){
			$query="SELECT c001t_empleados.co_empleado as idempleado, c001t_empleados.tx_nombre_empleado as nombreempleado,
					       c001t_empleados.tx_apellido_empleado as apellidoempleado,  
					       cu.nombre as curso, inscritos.fecha as fechainscripcion, inscritos.idinscrito,
					       categorias.nombrecategoria, inscritos.aprobado FROM inscritos 
					INNER JOIN c001t_empleados ON c001t_empleados.co_empleado=inscritos.idempleado
					INNER JOIN cursos AS cu ON cu.idcurso=inscritos.idcurso
					INNER JOIN categorias ON categorias.idcategoria=cu.idcategoria ORDER BY idinscrito DESC";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;		

		}
		public function cambiarFecha($fecha, $idcurso){			
			$this->salida="true";
			$query="UPDATE cursos SET  fecha='$fecha' WHERE idcurso='$idcurso'";
			$this->resul=$this->consultar($query);			
			return $this->salida;
		}
		public function obtenerEstados_cursos(){	
			$query="SELECT * FROM  estadoscurso ORDER BY idestado DESC ";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;
		}
		public function obtenerProponerCurso($idcurso){			
			$query="SELECT * FROM  cursos WHERE  idcurso='$idcurso'";
			$this->resul=$this->consultar($query);			
			return $this->resul;			
		}
		public function cambiarEstado($curso, $estado){				
			$this->salida="true";	
			
			if($estado==1){
				$BuscarEmpleado="SELECT iddictadopor FROM  cursos  WHERE  idcurso='$curso'";
				$this->resul2=$this->consultar($BuscarEmpleado);
				$rows[] = pg_fetch_array($this->resul2, null, PGSQL_ASSOC);
				$idempleado=$rows[0]['iddictadopor'];

				$buscarcursospropuestos="SELECT cursospropuestos FROM  controlempleado WHERE  idempleado='$idempleado'";
				$this->resul3=$this->ObtenerColumnas_consulta($buscarcursospropuestos);
				$cursosPropuestos=$this->resul3[1]['cursospropuestos'];
				/*echo "cursos propuestos ".$cursosPropuestos;*/
				if((int) $cursosPropuestos<3){

					/* Actualizo Control */
					$ActualizarControl="UPDATE controlempleado SET cursospropuestos=cursospropuestos+1  WHERE idempleado='$idempleado'";
					$this->resul4=$this->consultar($ActualizarControl);
					/* Actualizo Control */



					/* Apruebo Curso */
					$query="UPDATE cursos SET idestado='$estado' WHERE  idcurso='$curso'";
					$this->resul=$this->consultar($query);

					/* Apruebo Curso */

					
				}
				else{
					$this->salida="El empleado no puede superar los 3 cursos propuestos aprobados";
				}



			}	
			if($estado==2){
				$query="UPDATE cursos SET idestado='$estado' WHERE  idcurso='$curso'";
				$this->resul=$this->consultar($query);
				/*$rows[] = pg_fetch_array($this->resul2, null, PGSQL_ASSOC);
				$idempleado=$rows[0]['iddictadopor'];
				$ActualizarControl=" UPDATE controlempleado  SET cursospropuestos=cursospropuestos-1  WHERE idempleado='$idempleado'";
				$this->resul3=$this->consultar($ActualizarControl);*/
			}	
		
			return $this->salida;
		}
		public function ProponerCurso($nombre, $fecha, $sitio, $cupos, $dictadopor, $duracion, $categoria){			
			$this->salida="true";		
			$query="INSERT INTO cursos (nombre, fecha, sitio, cupos, iddictadopor, duracion, idestado, idcategoria)
					VALUES('$nombre', '$fecha', '$sitio', '$cupos', '$dictadopor', '$duracion', '3', '$categoria')";
			$this->resul=$this->consultar($query);		
			return $this->salida;
		}

		public function ObtenerPropuestas($estado){			
			$query="SELECT cursos.*, estadoscurso.nombre as nombreestado FROM  cursos  INNER JOIN estadoscurso on estadoscurso.idestado=cursos.idestado   ORDER BY idcurso DESC";
			$this->resul=$this->consultar($query);			
			return $this->resul;
		}

		public function ObtenerPropuestasAprobados($estado){			
			$query="SELECT cursos.*, estadoscurso.nombre as nombreestado FROM  cursos  INNER JOIN estadoscurso on estadoscurso.idestado=cursos.idestado WHERE  cursos.idestado='$estado'  ORDER BY idcurso DESC";
			$this->resul=$this->consultar($query);			
			return $this->resul;
		}


	}

/*$c=new Curso();
$c->cambiarEstado(9,1);*/
	
?>