var IDCURSO=-1;

(function main(){
	$("#wait").css("display", "block");
	obtenerCursosPropuestos(2);	
})();

function modificarcurso(){
	$('#modificar').on('click', function(){	
		console.log("Modificar curso");
		$("#wait").css("display", "block");
		var evento=function(msg){
		 	if(msg=="true"||msg==true){
		 		$("#wait").css("display", "none");	
		 		alerta.innerHTML="Se ha modificado correctamente la fecha del curso";
		 		notificar(IDCURSO, tfecha.value);
		 	}
		}
		var data={id:6, idcurso:IDCURSO, fecha:tfecha.value};
		ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Cambiar FECHA",  evento);

	});
}

function notificar(idcurso, fecha){
		var evento=function(msg){
		 	console.log(msg);
		}
		var data={id:2, idcurso:IDCURSO, fecha:fecha};
		ajax("../supervisor/controlador/CNotificacion.php", data, false,true, "Notificacion de Cambio de Fecha",  evento);
}
function obtenerCursosPropuestos(estado){
	var evento=function(msg){
		  $('#tabla').html("");   	 	                  
         ListarTabla(msg);          
         VermasDetalles();         
         cargarCategorias();
	}
	var data={id:2, estado:estado};
	ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Obtener cursos propuestos",  evento);
}

function cambiarEstado(){
	$('.estado').on('click', function(){	
		var id=$(this).attr('name');
		var valor=$(this).attr('value');
		console.log("Id: "+id+ " Value: "+valor);
		var evento=function(msg){
		 	if(msg=="true"||msg==true){
		 		$("#wait").css("display", "block");
				obtenerCursosPropuestos(2);	
		 	}
		}
		var data={id:3, idcurso:id, estado:valor};
		ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Cambiar Estado",  evento);		
	});
}
function cargarCategorias(){
		var evento=function(msg){
				    $('#Scategoria').empty();                
                     for (var i =0 ; i<msg[0].m; i++) {  
                          $('#Scategoria').append('<option value="'+msg[i].idcategoria+'" >'+msg[i].nombrecategoria+'</option>');
            		}
            		ObtenerEstaodos_cursos();
            		mostrardictadores();
                	
		}
		var data={id:1};
		ajax("../supervisor/controlador/Ccategorias.php", data, false, true, "Obtener Categorias",  evento);
}

function ObtenerEstaodos_cursos(){
		var evento=function(msg){
				console.log(msg.length);
				$('#Sestados2').empty();                    
				 for (var i =0 ; i<msg.length; i++) {  
				      $('#Sestados2').append('<option value="'+msg[i].idestado+'" >'+msg[i].nombre+'</option>');
				}           	
		}
		var data={id:5};
		ajax("../supervisor/controlador/Ccurso.php", data, false, true, "Obtener Estadoscurso",  evento);
}


function mostrardictadores(){
	var evento=function(msg){
				    $('#Sdictadopor').empty();                    
                     for (var i =0 ; i<msg[0].m; i++) {  
                          $('#Sdictadopor').append('<option value="'+msg[i].idempleado+'" >'+msg[i].nombreempleado+'</option>');
            		}
            		setTimeout(function(){
                		$("#wait").css("display", "none");
                	}, 1000);                	
		}
		var data={id:3};
		ajax("../supervisor/controlador/Cempleados.php", data, false, true, "Obtener Empleados que dictaran cursos",  evento);
}
function VermasDetalles(){
	$('.vermas').on('click', function(){
		$("#wait").css("display", "block");
		var id=$(this).attr('name');
		var options = { to: { width: 200, height: 60 } };
		var options2 = { to: { width: 280, height: 185 } };
		$('#resultado').hide();
		$('#alerta').show();
		$('#tabla').toggle("slide", options, 500, function(){
				var evento=function(msg){	
							IDCURSO=msg[0].idcurso;				 	
					 		tnombre.value=msg[0].nombre;
					 		tfecha.value=msg[0].fecha2;
					 		tsitio.value=msg[0].sitio;
					 		tcupos.value=msg[0].cupos;
					 		$('#Sdictadopor').val(msg[0].iddictadopor);
					 		tduracion.value=msg[0].duracion;
					 		$('#Scategoria').val(msg[0].idcategoria);
					 		Sestados2.value=msg[0].idestado;
					 		$("#wait").css("display", "none");						 			 	
				}
			var data={id:4, idcurso:id};
			ajax("../supervisor/controlador/Ccurso.php", data, false, true, "Obtener Curso",  evento);
			$('#formulario').show("slide", options2, 500);
		});
	});
}


	$('#volver').on('click', function(){
		var options = { to: { width: 200, height: 60 } };
		var options2 = { to: { width: 280, height: 185 } };		
		$('#resultado').show();
		$('#alerta').hide();	
		$('#formulario').toggle("slide", options2, 500, function(){			
			obtenerCursosPropuestos(2);
			$('#tabla').show("slide", options, 500);
		});		
	});
	modificarcurso();

function ListarTabla(msg){         
		var row1=$('<tr></tr>');
		var fila00=$('<th></th>').text("Nº");
		var fila0=$('<th></th>').text("Nombre");
		var fila1=$('<th></th>').text("Fecha");		
		var fila3=$('<th></th>').text("Cupos");
		var fila4=$('<th></th>').text("Estado");
		var fila5=$('<th></th>').text("Opciones");
		row1.append(fila00);
		row1.append(fila0);
		row1.append(fila1);		
		row1.append(fila3);
		row1.append(fila4); 
		row1.append(fila5); 
		$('#tabla').append(row1);
		resultado.innerHTML='Mostrando '+msg[0].m+' resultados encontrados';
        for(i=0; i<msg[0].m; i++){                   
                var row3=$('<tr></tr>');
                var fila00=$('<td></td>').text(msg[i].idcurso);
                var fila0=$('<td></td>').text(msg[i].nombre);
                var fila1 = $('<td></td>').text(msg[i].fecha);             
                var fila3 = $('<td></td>').text(msg[i].cupos);                
                var fila4 = "";
                if(msg[i].nombreestado=="APROBADO"){
                	fila4=$('<td></td>').html('<span class="label label-success estado" value="1" name="'+msg[i].idcurso+'">'+msg[i].nombreestado+' </span>');
                }
                else if(msg[i].nombreestado=="NO APROBADO"){
                	fila4=$('<td></td>').html('<span class="label label-danger estado"  value="0" name="'+msg[i].idcurso+'">'+msg[i].nombreestado+'</span>');
                }
                else if(msg[i].nombreestado=="EN ESPERA"){
                	fila4=$('<td></td>').html('<span class="label label-warning estado"  value="0" name="'+msg[i].idcurso+'">'+msg[i].nombreestado+'</span>');
                }
              
                var fila5 = $('<td></td>').html('<button class="vermas"  title="Ver mas detalles" name="'+msg[i].idcurso+'" >+</button>');

                row3.append(fila00);
                row3.append(fila0);
                row3.append(fila1);               
                row3.append(fila3);  
                row3.append(fila4);
                row3.append(fila5);
				$('#tabla').append(row3);           
        }		
}
