(function main(){
	console.log("Hola mundo : Aprobar Postulacion");
	obtenerPostulaciones();
})();

function obtenerPostulaciones(){
	var evento=function(msg){
		  console.log(msg);
		  $('#tabla').html("");   	 	                  
          ListarTabla(msg); 
          Aprobar();
          liberar();
          verEmpleado();                	  
	}
	var data={id:7};
	ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Obtener Postulaciones",  evento);
}

function Aprobar(){
	$('.aprobar').on('click', function(){
		$("#wait").css("display", "block");
		var idinscrito=$(this).attr('name');
		var data="";
		var aprobado="";
		if($(this).is(':checked')) {  
			 aprobado=1;
             data={id:8, idinscrito:idinscrito, aprobado:aprobado};
             var x = document.getElementsByName("liberar"+idinscrito);
             console.log(x);
             $(x).hide();
             
        } else {  
        	aprobado=2;
             data={id:8, idinscrito:idinscrito, aprobado:aprobado};
             var x = document.getElementsByName("liberar"+idinscrito);
             console.log(x);
             $(x).show();
        }  

		var evento=function(msg){
			if(msg=="true"||msg==true){
				$("#wait").css("display", "none");
				notificar(idinscrito);
				if(aprobado==1){
					alert("Se ha Aprobado la solicitud de postulacion");					
				}
				else{
					alert("Se ha desaprobado la solicitud de postulacion");
				}
		 	}
		 	else{
				$("#wait").css("display", "none");
				
		 		alert(msg);
		 		obtenerPostulaciones();

		 	}
		}
		
		ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Aprobar Postulacion",  evento);	
	});
}

function liberar(){
	$('.liberar').on('click', function(){
		$("#wait").css("display", "block");
		var idinscrito=$(this).attr('name');
		console.log("idinscrito: "+idinscrito.length);
		var idnuevo="";
		for (var i =7; i <idinscrito.length; i++) {
			idnuevo+=idinscrito[i];
		}
		console.log("nuevo id: "+idnuevo);
		data={id:10, idinscrito:idnuevo};
		var evento=function(msg){
			console.log("salida: " +msg);
			if(msg=="true"||msg==true){
				$("#wait").css("display", "none");
				obtenerPostulaciones();

			}
		}
		
		ajax("../supervisor/controlador/Ccurso.php", data, false,true, "Liberar cupo",  evento);	
	});

}

function notificar(idinscrito){
		var evento=function(msg){
		 	console.log(msg);
		}
		var data={id:4, idinscrito:idinscrito};
		ajax("../supervisor/controlador/CNotificacion.php", data, false,false, "Notificacion de aprobacion de postulacion",  evento);
}

function verEmpleado(){
	$('.empleado').on('click', function(){
		var idempleado=$(this).attr('name');
		window.open("controlempleado.php?id="+idempleado, "", "left=100, top=100  width=850, height=400");
	});

}	

function ListarTabla(msg){         
		var row1=$('<tr></tr>');
		var fila0=$('<th></th>').text("Cod. Empleado ");
		var fila1=$('<th></th>').text("Nombre Empleado");		
		var fila2=$('<th></th>').text("Curso");
		var fila3=$('<th></th>').text("Categoria");
		var fila4=$('<th></th>').text("Fecha de postulacion");
		var fila5=$('<th></th>').text("Aprobar");
		var fila6=$('<th></th>').text("Liberar");

		row1.append(fila0);
		row1.append(fila1);		
		row1.append(fila2);
		row1.append(fila3); 
		row1.append(fila4);
		row1.append(fila5); 
 		row1.append(fila6);
		$('#tabla').append(row1);
		resultado.innerHTML='Mostrando '+msg[0].m+' resultados encontrados';
        for(i=1; i<=msg[0].m; i++){                   
                var row3=$('<tr></tr>');
                var fila0=$('<td></td>').html('<a href="#" class="empleado" name="'+msg[i].idempleado+'">'+msg[i].idempleado+'</a>');
                var fila1 = $('<td></td>').text(msg[i].nombreempleado+ " "+msg[i].apellidoempleado);             
                var fila2 = $('<td></td>').text(msg[i].curso);                
                var fila3 = $('<td></td>').text(msg[i].nombrecategoria);                
                var fila4= $('<td></td>').text(msg[i].fechainscripcion);                
                var fila5="";
                var fila6="";
                console.log("inscrito aprobado? "+msg[i].aprobado);
               	if(msg[i].aprobado==0){
                	 fila5 = $('<td></td>').html('<input  type="checkbox" class="aprobar" name="'+msg[i].idinscrito+'" >'); 
                	 fila6= $('<td></td>').html('<input name="liberar'+msg[i].idinscrito+'" style="display:block;" class="liberar" type="button"  value="Liberar"/>');
               	}
               	else{
                	 fila5 = $('<td></td>').html('<input  type="checkbox" class="aprobar" name="'+msg[i].idinscrito+'" checked>');
                	 fila6= $('<td></td>').html('<input name="liberar'+msg[i].idinscrito+'" style="display:none;" class="liberar" type="button"  value="Liberar"/>');
               	}
               
               	//fila5.append();

                row3.append(fila0);
                row3.append(fila1);               
                row3.append(fila2);  
                row3.append(fila3);
                row3.append(fila4);
                row3.append(fila5);
                row3.append(fila6);
				$('#tabla').append(row3);           
        }		
}
