<?php
	
	
	require_once "conexion.php";
	session_start();

	class Notificacion extends BasedeDatos {
		public function EnviarNotificacion3($idinscrito, $mensaje){
			$query="SELECT idempleado FROM inscritos WHERE  idinscrito='$idinscrito'";
			$this->resul=$this->ObtenerColumnas_consulta($query);
			$fecha=date("Y-m-d");
			$query2="INSERT INTO notificaciones (mensaje, fecha)  VALUES ('$mensaje', '$fecha') RETURNING idnotificacion";
			$this->resul2=$this->consultar($query2);	
			$insert_row = pg_fetch_row($this->resul2);
			$idnotificacion = $insert_row[0];
			for ($i=1; $i<sizeof($this->resul); $i++) {
				$idempleado=$this->resul[$i]['idempleado'];
				$query3="INSERT INTO enviar_notificacion (idempleado, idnotificacion, idestadonotificacion) VALUES ('$idempleado', '$idnotificacion', '2')";
				$this->resul3=$this->consultar($query3);	

			}
		}

		public function EnviarNotificacion2($idcurso, $estado){
			$query="SELECT iddictadopor FROM cursos WHERE  idcurso='$idcurso'";
			$this->resul=$this->ObtenerColumnas_consulta($query);
			$fecha=date("Y-m-d");
			$datoscurso="SELECT nombre FROM  cursos WHERE  idcurso='$idcurso'";
			$this->resul2=$this->ObtenerColumnas_consulta($datoscurso);
			$mensaje2="";
			if($estado==1||$estado=="1")
				$mensaje2="Se le notifica que el curso ".$this->resul2[1]['nombre']." se le cambio el estado de curso a APROBADO ";
			if($estado==2||$estado=="2")
				$mensaje2="Se le notifica que el curso ".$this->resul2[1]['nombre']." se le cambio el estado de curso a NO APROBADO ";


			$query2="INSERT INTO notificaciones (mensaje, fecha, idcurso)  VALUES ('$mensaje2', '$fecha', '$idcurso') RETURNING idnotificacion";
			$this->resul2=$this->consultar($query2);	
			$insert_row = pg_fetch_row($this->resul2);
			$idnotificacion = $insert_row[0];
			

			for ($i=1; $i<sizeof($this->resul); $i++) {
				$idempleado=$this->resul[$i]['iddictadopor'];
				$query3="INSERT INTO enviar_notificacion (idempleado, idnotificacion, idestadonotificacion) VALUES ('$idempleado', '$idnotificacion', '2')";
				$this->resul3=$this->consultar($query3);	

			}

		}
		public function EnviarNotificacion($idcurso, $fecha2){
			$query="SELECT idempleado FROM inscritos WHERE  idcurso='$idcurso'";
			$this->resul=$this->ObtenerColumnas_consulta($query);
			$fecha=date("Y-m-d");


			$datoscurso="SELECT nombre FROM  cursos WHERE  idcurso='$idcurso'";
			$this->resul5=$this->ObtenerColumnas_consulta($datoscurso);

			$mensaje2="Se le Notifica que se ha cambiado la fecha de inicio del curso  ".$this->resul5[1]['nombre']." a la siguiente fecha: ".$fecha2 ;

			$query2="INSERT INTO notificaciones (mensaje, fecha, idcurso)  VALUES ('$mensaje2', '$fecha', '$idcurso') RETURNING idnotificacion";
			$this->resul2=$this->consultar($query2);	
			$insert_row = pg_fetch_row($this->resul2);
			$idnotificacion = $insert_row[0];
			
			
			for ($i=1; $i<sizeof($this->resul); $i++) {
				$idempleado=$this->resul[$i]['idempleado'];
				$query3="INSERT INTO enviar_notificacion (idempleado, idnotificacion, idestadonotificacion) VALUES ('$idempleado', '$idnotificacion', '2')";
				$this->resul3=$this->consultar($query3);	

			}

			$query="SELECT iddictadopor FROM cursos WHERE  idcurso='$idcurso'";
			$this->resul=$this->ObtenerColumnas_consulta($query);
			for ($i=1; $i<sizeof($this->resul); $i++) {
				$idempleado=$this->resul[$i]['iddictadopor'];
				$query3="INSERT INTO enviar_notificacion (idempleado, idnotificacion, idestadonotificacion) VALUES ('$idempleado', '$idnotificacion', '2')";
				$this->resul3=$this->consultar($query3);	

			}


		}
		public function ObtenerNotificaciones(){
			$empleado=$_SESSION['co_empleado'];

			$query="SELECT enviar_notificacion.*, n.* FROM enviar_notificacion 
					INNER JOIN notificaciones AS n ON  n.idnotificacion=enviar_notificacion.idnotificacion
					INNER JOIN estado_notificacion AS en ON en.idestadonotificacion=enviar_notificacion.idestadonotificacion
					WHERE  idempleado='$empleado' ORDER BY  idenviarnotificacion DESC";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;
		}
	}


/*	$n=new Notificacion();
	echo $n->EnviarNotificacion(38,"2015-01-01");*/
?>