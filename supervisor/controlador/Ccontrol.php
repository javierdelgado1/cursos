<?php
	require_once "../modelo/control.php";
	if(isset($_REQUEST['id'])){
		$i=new Ccontrol();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$i->ObtenerControl();				
				break;
			case 2:
				$i->ObtenerCursosRealizados();
				break;
			case 3:
				$i->ObtenerCursosdictados();
				break;
			
			default;
		}
	}
	class Ccontrol {
		public $c;

		public  function __construct(){
			$this->c=new Control();
		}

		public function ObtenerCursosdictados(){
			$resul=$this->c->ObtenerCursosdictados();
			echo json_encode($resul);
		}
		public function ObtenerCursosRealizados(){
			$resul=$this->c->ObtenerCursosRealizados();
			echo json_encode($resul);			
		}

		public function ObtenerControl(){
			$resul=$this->c->ObtenerControl();
			echo json_encode($resul);
		} 

   }
?>