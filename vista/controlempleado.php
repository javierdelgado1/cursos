<?php
   session_start();

   $x=$_GET['id'];
   $_SESSION['idempleadoControlEmpleado']=$x; 

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <title>Aplicaciones de PDVSA </title>
    <link rel="stylesheet" title="Principal-Aplicaciones" type="text/css" href="css/main-aplicacion.css" />   
</head>
<body>  
      <div id="Main">
       <div class="Contenedor" id="Main-header">
             <a href=""><span class="Contenedor-con-Imagen" id="Main-Logo"></span></a>
			<div class="Contenedor" id="Contenedor-Degradado">
			    <div class="Contenedor-con-Imagen" id="Logo-Continuacion">
			    	<span class="Contenedor" id="Nombre-Aplicacion">Sistemas de Cursos</span>
				 </div> 
				 <div class="Contenedor-con-Imagen" id="Logo-Final"></div>
			</div>
       </div>
       <div class="Contenedor-con-sombra" id="Main-BackCuerpoRight">
            <div class="Contenedor-con-sombra" id="Main-BackCuerpoLeft"> 
                <div class="Contenedor" id="Main-Cuerpo">

					<div class="Contenedor-con-Bordes" id="Main-Identificador_usuario">
                        <span class="Texto-Identificador" id="Main-Usuario">Control de Empleado</span>
                        <span class="Texto-Identificador" id="Main-Fecha"><?php  $d=date("d-m-Y"); echo $d; ?></span>
                    </div>
                    
                    <div class="Contenedor" id="Main-breadcrumbs">
                        <div id="Main-Traza"><div id="Main-Traza-aux"></div></div>
						<div class="EsquinasBread" id="EsquinaBreadDerecha"></div>
					</div>
					
					<div class="Contenedor-Editable-Fondo" id="Vista">
      
					    <div class="Contenedor-Editable" id="Menu">      
                  			<a class="Contenedor-Texto-Menu" id="cerrarSesion"><span class="Text-menu" >Cerrar</span></a>
					    </div>    
              			
              			<div id="wait" style="display: none; position: absolute; top: 38%; left: 54%; z-index: 1000;"><img src="imagenes-aplicacion/ajax-loader.gif" width="54" height="54"><center><small>Cargando</small></center></div>

					   <!--aqui comenzaria la region editable si se hace una plantilla-->
				       <div class="Contenedor-Editable" id="Region-Editable">

					   </div>
						<!--aqui terminaria la region editable si se hace una plantilla-->
  					 </div>
 				   </div>
  		    	</div>
          </div>
         <div class="Contenedor-con-Bordes" id="Main-Contenedor-footer"></div>
        </div> 
   	 <script type="text/javascript" src="include/jquery-2.1.1.min.js"></script>
     <script type="text/javascript" src="../supervisor/controlador/eventosscript/funcionescomunes.js"></script> 
     <script type="text/javascript" src="../supervisor/controlador/eventosscript/supervisor/controlempleado.js"></script>

   </body>
</html>

