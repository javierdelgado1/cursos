<?php

abstract class BasedeDatos{


	protected $host; 
	protected $db;
	protected $user; 
	protected $pass; 
	protected $conexion; 
	protected $url; 

	public function __construct(){

	}

	public function cargarValores(){
		$this->host='localhost';
		$this->db='cursos';
		$this->user='postgres';
		$this->pass='123';		
		$this->conexion="user='$this->user'  password='$this->pass' port=5432  dbname='$this->db' host='$this->host'";
	}
	private function conectar(){
		$this->cargarValores();
		$this->url=pg_connect($this->conexion);
		return true;
	}
	private function destruir(){
		pg_close($this->url);
	}
	public function getConexion(){
		return $this->url;
	}

	public function consultar($query){
		$this->conectar();
		$result=pg_query($this->url, $query);
		$this->destruir();
		return $result;
	}

	protected function ObtenerColumnas_consulta($query){
		$this->conectar();
		$rows = array();
		$result =pg_query($this->url, $query);
		$rows[0]['m']=pg_num_rows($result);
		while ($rows[] = pg_fetch_array($result, null, PGSQL_ASSOC));		
		$this->destruir();
		array_pop($rows);
		return $rows;
	}
}


	/*$conexion = new ConexionPGSQL();



	if($conexion->conectar()==true){

		echo "conexion exitosa";

	}else{

		echo "no se pudo conectar";

	}

	$query="SELECT * FROM c001t_empleados";
	$query2="INSERT INTO c001t_empleados (co_empleado) VALUES(1)";
	$result=$conexion->consultar($query);
	echo "numero de tuplas retornadas: ".pg_numrows($result); */

?>