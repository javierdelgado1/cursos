(function main(){
	$("#wait").css("display", "none");
	cargarCategorias();

	$('#aceptar').on('click', function(){
		//console.log("validar: "+validar());
		if(validar()){
			alerta.innerHTML="";
			var evento=function(msg){
					console.log(msg);
					if(msg==true||msg=="true"){
						limpiarFormulario();
						alerta.innerHTML="Se ha registrado correctamente el curso que ha propuesto";
					}
					if(msg=="repetido"){						
						alerta.innerHTML="¡Disculpe ya existe un curso propuesto para la fecha "+tfecha.value + " por favor elija otra fecha distinta!";
					}
	                	
			}
			var data={id:1, nombre:tnombre.value, fecha:tfecha.value, sitio:tsitio.value, cupos:tcupos.value, duracion:tduracion.value, categoria:$('#Scategoria').val()};
			ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Proponer curso",  evento);
					
		}
	});

	$('#cancelar').on('click', function(){
		limpiarFormulario();

	});


})();


function limpiarFormulario(){
	tnombre.value="";
	tfecha.value="";
	tsitio.value="";
	tcupos.value="";
	/*Sdictadopor.value="-1";*/
	tduracion.value="";
	Scategoria.value="-1"

}

function cargarCategorias(){
		var evento=function(msg){
				//console.log(msg);
				    $('#Scategoria').empty();           
                     $('#Scategoria').append(' <option value="-1" disabled selected> --Categorias -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                          $('#Scategoria').append('<option value="'+msg[i].idcategoria+'" >'+msg[i].nombrecategoria+'</option>');
            		}
            		setTimeout(function(){
                		$("#wait").css("display", "none");
                	}, 1000);
            		//mostrardictadores();
                	
		}
		var data={id:1};
		ajax("../trabajador/controlador/Ccategorias.php", data, false, true, "Obtener Categorias",  evento);
}

function mostrardictadores(){
	var evento=function(msg){
				//console.log(msg);
				    $('#Sdictadopor').empty();           
                     $('#Sdictadopor').append(' <option value="-1" disabled selected> --Empleado -- </option>');
                     for (var i =0 ; i<msg[0].m; i++) {  
                          $('#Sdictadopor').append('<option value="'+msg[i].idempleado+'" >'+msg[i].nombreempleado+'</option>');
            		}
            		setTimeout(function(){
                		$("#wait").css("display", "none");
                	}, 1000);
                	
		}
		var data={id:3};
		ajax("../../trabajador/controlador/Cempleados.php", data, false, true, "Obtener Empleados que dictaran cursos",  evento);
}
		




function validar(){
		var salida=false;
	/*	console.log("dictado por: " +Sdictadopor.value  + "  :  "+ $('#Sdictadopor').val());*/
		/*console.log("categoria : " +Scategoria.value  + "  :  "+ $('#Scategoria').val());*/

		if($('#tnombre').val()==""){			 
	        alerta.innerHTML="¡Por favor escriba el <b>Nombre </b>del curso!";        
	        return false;		
	    }
	   
	     else if($('#tfecha').val()=="")
	    {  
	        alerta.innerHTML="¡Por favor ingrese la  <b>Fecha </b> del curso!";
	        return false;
	    }
	     else if($('#tsitio').val()=="")
	    {  
	        alerta.innerHTML="¡Por favor ingrese el  <b>sitio </b> donde se dictara el curso!";
	        return false;
	    }

	     else if($('#tcupos').val()=="")
	    {  
	        alerta.innerHTML="¡Por favor ingrese los  <b>cupos</b> para el curso!";
	        return false;
	    }

	    /* else if(Sdictadopor.value=="-1")
	    {  
	        alerta.innerHTML="¡Por favor seleccione  quien <b>dictara</b> el curso!";
	        return false;
	    }*/
	      else if($('#tduracion').val()=="")
	    {  
	        alerta.innerHTML="¡Por favor debe escribir la  <b>duracion</b> del curso!";
	        return false;
	    }
	     else if(Scategoria.value=="-1")
	    {  
	        alerta.innerHTML="¡Por favor seleccione  la <b>categoria</b> del curso!";
	        return false;
	    }
	   
	    else {salida=true;}	

		return salida;
}