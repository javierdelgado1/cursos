<?php	

	require_once "conexion.php";
	session_start();
	class Control extends BasedeDatos {

		public function ObtenerCursosdictados(){
			$idempleado=$_SESSION['idempleadoControlEmpleado'];
			$query="SELECT nombre FROM  cursos  WHERE  iddictadopor='$idempleado' AND idestado='1'";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;

		}
		public function ObtenerCursosRealizados()
		{
			$idempleado=$_SESSION['idempleadoControlEmpleado'];
			$query="SELECT cursos.* FROM  inscritos 
					INNER JOIN cursos ON cursos.idcurso=inscritos.idcurso WHERE  inscritos.idempleado='$idempleado' AND inscritos.aprobado='1'";
			$this->resul=$this->ObtenerColumnas_consulta($query);			
			return $this->resul;
		}
		public function ObtenerControl()
		{
			
			$idempleado=$_SESSION['idempleadoControlEmpleado'];
			$query="SELECT controlempleado.*, c001t_empleados.co_empleado as idempleado2, c001t_empleados.tx_nombre_empleado as nombreempleado, 	c001t_empleados.tx_apellido_empleado as apellidoempleado FROM  controlempleado INNER JOIN c001t_empleados ON c001t_empleados.co_empleado=controlempleado.idempleado WHERE  controlempleado.idempleado='$idempleado'";
			$this->resul=$this->ObtenerColumnas_consulta($query);	

			if($this->resul[0]['m']==0){
				$query2="SELECT * FROM c001t_empleados WHERE  co_empleado='$idempleado'";
				$this->resul=$this->ObtenerColumnas_consulta($query2);	
				$this->resul[0]['m']=-1;
			}
					

			return $this->resul;	

		}
	}
?>