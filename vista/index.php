<?php
    session_start(); 
    if(!isset($_SESSION['co_empleado']))
    {
    	header('Location: ../login.html');
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
<head>
    <title>Aplicaciones de PDVSA </title>
    <link rel="stylesheet" title="Principal-Aplicaciones" type="text/css" href="css/main-aplicacion.css" />   
</head>
<body>  
      <div id="Main">
       <div class="Contenedor" id="Main-header">
             <a href=""><span class="Contenedor-con-Imagen" id="Main-Logo"></span></a>
			<div class="Contenedor" id="Contenedor-Degradado">
			    <div class="Contenedor-con-Imagen" id="Logo-Continuacion">
			    	<span class="Contenedor" id="Nombre-Aplicacion">Sistemas de Cursos</span>
				 </div> 
				 <div class="Contenedor-con-Imagen" id="Logo-Final"></div>
			</div>
       </div>
       <div class="Contenedor-con-sombra" id="Main-BackCuerpoRight">
            <div class="Contenedor-con-sombra" id="Main-BackCuerpoLeft"> 
                <div class="Contenedor" id="Main-Cuerpo">

					<div class="Contenedor-con-Bordes" id="Main-Identificador_usuario">
                        <span class="Texto-Identificador" id="Main-Usuario"> Bienvenido, <?php echo $_SESSION['tx_nombre_empleado']."  ".$_SESSION['tx_apellido_empleado']; ?> </span>
                        <span class="Texto-Identificador" id="Main-Fecha"><?php  $d=date("d-m-Y"); echo $d; ?></span>
                    </div>
                    
                    <div class="Contenedor" id="Main-breadcrumbs">
                        <div id="Main-Traza"><div id="Main-Traza-aux"></div></div>
						<div class="EsquinasBread" id="EsquinaBreadDerecha"></div>
					</div>
					
					<div class="Contenedor-Editable-Fondo" id="Vista">
          <?php 
            
          if(isset($_SESSION['co_roles'])&&$_SESSION['co_roles']==1)  { ?>
					    <div class="Contenedor-Editable" id="Menu">
               
                  <a href="" class="Contenedor-Texto-Menu" id="postularseacurso"><span class="Text-menu" ><b>Postularse a Curso</b></span></a>
                  <a href="" class="Contenedor-Texto-Menu" id="proponercurso"><span class="Text-menu" ><b>Proponer Curso</b></span></a>
                <span class="PuntoHo_Cortico"></span>
                   <a href="" class="Contenedor-Texto-Menu"><span class="Text-menu" ><b>Buscar Cursos</b></span></a>
                  <a class="Contenedor-Texto-sub-Menu"  id="Bporfecha"><span class="Text-menu" ><small>Por fechas</small></span></a>
                  <a class="Contenedor-Texto-sub-Menu"  id="Bporcategoria"><span class="Text-menu" ><small>Por categorias</small></span></a>
                  <a class="Contenedor-Texto-sub-Menu"  id="Bpornombre"><span class="Text-menu" ><small>Por nombre</small></span></a>

                
                <span class="PuntoHo_Cortico"></span>
                  <a href="" class="Contenedor-Texto-Menu" id="vernotificaciones"><span class="Text-menu" ><b>Ver Notificaciones</b></span></a>

                <span class="PuntoHo_Cortico"></span>
                  <a class="Contenedor-Texto-Menu" id="cerrarSesion"><span class="Text-menu" >Cerrar Sesi�n</span></a>
					    </div>
          <?php } if(isset($_SESSION['co_roles'])&&$_SESSION['co_roles']==2) { ?>



              <div class="Contenedor-Editable" id="Menu">

              <a href="" class="Contenedor-Texto-Menu"><span class="Text-menu" ><b>Gestionar Cursos</b></span></a>
                <a class="Contenedor-Texto-sub-Menu"  id="aprobarpostulacion"><span class="Text-menu" ><small>Aprobar Postulacion</small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="modificarfechas"><span class="Text-menu" ><small>Modificar Fechas </small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="asignarestatus"><span class="Text-menu" ><small>Asignar Status </small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="listarcursos"><span class="Text-menu" ><small>Listar Cursos</small></span></a>
                <span class="PuntoHo_Cortico"></span>                
                <a href="" class="Contenedor-Texto-Menu"><span class="Text-menu" ><b>Gestionar Reportes</b></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="Rcursos"><span class="Text-menu" ><small>Cursos</small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="RcursosRealizados"><span class="Text-menu" ><small>Cursos Realizados</small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="RCursosNoRealizados"><span class="Text-menu" ><small>Cursos No Realizados</small></span></a>
                <a class="Contenedor-Texto-sub-Menu" id="Rrealizadosportrabajadores"><span class="Text-menu" ><small>Realizados por Trabajadores</small></span></a>
               <!--  <span class="PuntoHo_Cortico"></span>                
                <a class="Contenedor-Texto-Menu" id="verTrabajadores"><span class="Text-menu" >Ver trabajadores</span></a>
 -->

                <span class="PuntoHo_Cortico"></span>                
                <a class="Contenedor-Texto-Menu" id="cerrarSesion"><span class="Text-menu" ><b>Cerrar Sesi�n</b></span></a>
              </div>

          <?php  } ?>
              			
              			<div id="wait" style="display: none; position: absolute; top: 38%; left: 54%; z-index: 1000;"><img src="imagenes-aplicacion/ajax-loader.gif" width="54" height="54"><center><small>Cargando</small></center></div>

					   <!--aqui comenzaria la region editable si se hace una plantilla-->
				       <div class="Contenedor-Editable" id="Region-Editable">


					   </div>
						<!--aqui terminaria la region editable si se hace una plantilla-->
  					 </div>
 				   </div>
  		    	</div>
          </div>
         <div class="Contenedor-con-Bordes" id="Main-Contenedor-footer"></div>
        </div> 
   	 <script type="text/javascript" src="include/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="../trabajador/controlador/eventosscript/eventosindex.js"></script>
   </body>
</html>

