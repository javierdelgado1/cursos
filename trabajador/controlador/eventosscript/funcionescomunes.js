function ajax( url, data, async, json, nombre, evento){
	//var MSG="";
	if(json){
		$.ajax
			({
			type: "POST",
		   	url: url,
		   	data: data,
			async: async,
			dataType: "json", 				
			success:
		    function (msg) 
			{	
				//MSG=msg;
				evento(msg);
				
		     },
		        error: function(jqXHR, textStatus, errorThrown) {
				  console.log(textStatus, errorThrown + " Error en: "+nombre);
				}
			});	
	}
	else{
		$.ajax
			({
			type: "POST",
		   	url: url,
		   	data: data,
			async: async,
							
			success:
		    function (msg) 
			{	
				//MSG=msg;
				evento(msg);
		     },
		        error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown + " Error en: "+nombre);
				}
			});	
	}
	//return MSG;

}