<?php
	require_once "../modelo/sesion.php";
	$id=$_REQUEST['id'];
	switch($id)
	{
		case 1:
			$empleado=$_REQUEST['codigo'];
			$pass=$_REQUEST['pass'];
			$s=new Csesion();
			$s->iniciarsesion($empleado,$pass);
			break;
		case 2:
			$s=new Csesion();
			$s->cerrarSesion();
			break;
		default;
	}


	class Csesion {

		public function cerrarSesion(){
				session_start();
				$_SESSION = array();

				// Si se desea destruir la sesión completamente, borre también la cookie de sesión.
				// Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
				if (ini_get("session.use_cookies")) {
				    $params = session_get_cookie_params();
				    setcookie(session_name(), '', time() - 42000,
				        $params["path"], $params["domain"],
				        $params["secure"], $params["httponly"]
				    );
				}

				// Finalmente, destruir la sesión.
				session_destroy();
			echo json_encode("true");

		}
		public function iniciarsesion($empleado, $password)
		{
			$sesion=new Sesion();
			$result=$sesion->ComprobarDatos($empleado);
			$objeto[0]['error']=true;

			if(pg_numrows($result)==0){
				$objeto[0]['error']=true;
			}
			else{
				
				$row=pg_fetch_assoc($result);
				if($row['co_contrasena']==$password){
					$objeto[0]['error']=false;
					session_start();
					
					$_SESSION['co_empleado']=$row['co_empleado'];	
					$_SESSION['co_area']=$row['co_area'];				
					$_SESSION['tx_nombre_empleado']=$row['tx_nombre_empleado'];				
					$_SESSION['tx_apellido_empleado']=$row['tx_apellido_empleado'];				
					$_SESSION['tx_indicador']=$row['tx_indicador'];	
					$_SESSION['co_roles']=$row['co_roles'];				
					$_SESSION['co_contrasena']=$row['co_contrasena'];	

					$objeto[0]['co_empleado']=$_SESSION['co_empleado'];	
					$objeto[0]['co_area']=$_SESSION['co_area'];				
					$objeto[0]['tx_nombre_empleado']=$_SESSION['tx_nombre_empleado'];				
					$objeto[0]['tx_apellido_empleado']=$_SESSION['tx_apellido_empleado'];				
					$objeto[0]['tx_indicador']=$_SESSION['tx_indicador'];	
					$objeto[0]['co_roles']=$_SESSION['co_roles'];				
					$objeto[0]['co_contrasena']=$_SESSION['co_contrasena'];	
								



				}
			}

			echo json_encode($objeto);
		}

	}

	/*$c=new Csesion();
	print_r($c->iniciarsesion("123", "123"));*/
	//echo $_SESSION['tx_apellido_empleado'];
?>