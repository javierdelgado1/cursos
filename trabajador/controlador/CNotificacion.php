<?php
	require_once "../modelo/notificaciones.php";
	if(isset($_REQUEST['id'])){
		$i=new CNotificacion();
		$id=$_REQUEST['id'];
		switch($id)
		{
			case 1:
				$i->ObtenerNotificaciones();	
				break;
			case 2:
				$mensaje="";
				$idcurso=$_REQUEST['idcurso'];				
				$i->EnviarNotificacion($idcurso, $mensaje);	
				break;
			
			default;
		}
	}
	class CNotificacion {
		public $n;

		public  function __construct(){
			$this->n=new Notificacion();
		}

		public function EnviarNotificacion($idcurso, $mensaje){
			$resul=$this->n->EnviarNotificacion($idcurso, $mensaje);
			echo json_encode($resul);
		}

		public function ObtenerNotificaciones(){
			$resul=$this->n->ObtenerNotificaciones();
			echo json_encode($resul);
		}

	}

?>