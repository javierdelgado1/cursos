(function main(){
	/*console.log("hola mundo: buscarpornombre");*/
	buscar();
})();
function buscar(){
	$('#aceptar').on('click', function(){
		if(validarNombre()){
        	alerta.innerHTML="";
			$("#wait").css("display", "block");

			var evento=function(msg){
				 /*console.log(msg);*/
				 $('#tabla').html(""); 
				 if(msg[0].m>0){
				 	ListarTabla(msg);  	                	
				 }
				 else{
					 $('#tabla').html(""); 
					resultado.innerHTML='No se encontraron resultados';
        				$("#wait").css("display", "none");

				 }  	 	                  
			}
			var data={id:12, nombre:tnombre.value}
			ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Buscar Por Nombre",  evento);
		}

	});

}
function validarNombre(){
	var salida=false;

	if(tnombre.value==""){
		alerta.innerHTML="¡Por favor escriba el nombre del curso!";        
        return false;		
	}
    else {salida=true;}	

	return salida;
}


function ListarTabla(msg){         
		var row1=$('<tr></tr>');
		var fila00=$('<th></th>').text("Nº");
		var fila0=$('<th></th>').text("Nombre");
		var fila1=$('<th></th>').text("Fecha");		
		var fila3=$('<th></th>').text("Cupos Disponibles");
		var fila3a=$('<th></th>').text("Sitio");
		var fila3b=$('<th></th>').text("Duracion");

		var fila4=$('<th></th>').text("Dictado por");
		var fila5=$('<th></th>').text("¿Inscrito?");
		row1.append(fila00);
		row1.append(fila0);
		row1.append(fila1);		
		row1.append(fila3);
		row1.append(fila3a);
		row1.append(fila3b);
		row1.append(fila4); 
		row1.append(fila5); 
		$('#tabla').append(row1);
		resultado.innerHTML='Mostrando '+msg[0].m+' resultados encontrados';
        for(i=1; i<=msg[0].m; i++){                   
                var row3=$('<tr></tr>');
                var fila00=$('<td></td>').text(msg[i].idcurso);
                var fila0=$('<td></td>').text(msg[i].nombre);
                var fila1 = $('<td></td>').text(msg[i].fecha); 
                var cupos=40;
                var evento=function(msg2){
					cupos=cupos-msg2[0].m;
				}
				var data={id:8, idcurso:msg[i].idcurso};
				ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Obtener inscritos dado un curso",  evento);           
                var fila3 = $('<td></td>').text(cupos);
                /*console.log("Sitio: " +msg[i].sitio + " Duracion: "+msg[i].duracion );*/
                var fila3a = $('<td></td>').text(msg[i].sitio);
                var fila3b = $('<td></td>').text(msg[i].duracion);

                var fila4 = $('<td></td>').text(msg[i].nombredictado + " "+ msg[i].apellidodictado);                
                
                var inscrito=false;
				 var fila5 = "";
              	var evento=function(msg3){
					//console.log(msg3);
					if(msg3[0].m>0){
                		inscrito=true;

					}
					if(inscrito){
	                	fila5 = $('<td></td>').html('<button   title="Inscrita"  disabled="true">Inscrita</button>');

					}	
					else{
	                	fila5 = $('<td></td>').html('<button   title="No Inscrita" disabled="true">No Inscrita</button>');

					}					
				}
				var data={id:10, idcurso:msg[i].idcurso};
				ajax("../trabajador/controlador/Ccurso.php", data, false,true, "Verificar si esta inscrito",  evento); 

                row3.append(fila00);
                row3.append(fila0);
                row3.append(fila1);               
                row3.append(fila3);
                row3.append(fila3a);  
                row3.append(fila3b);  

                row3.append(fila4);
                row3.append(fila5);
				$('#tabla').append(row3);    
				//break;       
        }		
		$("#wait").css("display", "none");
        
}